const { app, BrowserWindow } = require('electron');
const path = require('path');
app.whenReady().then(() => {
  const win = new BrowserWindow({
    webPreferences: {
        nodeIntegration: true,
        contextIsolation: false,
      preload: __dirname + '/preload.js'
    }
  });
  const dev = false;
  if(dev){
    win.loadFile('http://localhost:3000');
  }else{
    win.loadURL(
        `file://${path.join(__dirname, '../build/index.html')}`
    )
    win.loadFile(     
    `${path.join(__dirname, '../build/index.html')}`
    )
  }
  
  win.webContents.openDevTools()
})
app.on('window-all-closed', () => {
    app.quit()
})